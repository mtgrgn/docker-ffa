FROM jgoerzen/debian-base-standard

RUN \
  apt-get update &&\
  apt-get install -y \
    gnat-6 \
    gnupg \
    gprbuild \
    patch \
  || exit 1

WORKDIR /w/.wot
ADD ./.wot /w/.wot
#wget http://wot.deedbot.org/17215D118B7239507FAFED98B98228A001ABFFC7.asc -O asciilifeform.asc &&\
#wget http://wot.deedbot.org/027A8D7C0FB8A16643720F40721705A8B71EADAF.asc -O mod6.asc &&\
RUN \
  sha256sum * | diff - .sha256sums &&\
  gpg --import asciilifeform.asc &&\
  gpg --import mod6.asc \
  || exit 1

WORKDIR /w/v
ADD ./v /w/v
#wget http://thebitcoin.foundation/v/V-20170317.tar.gz &&\
#wget http://thebitcoin.foundation/v/V-20170317.tar.gz.mod6.sig &&\
RUN \
  sha256sum * | diff - .sha256sums &&\
  gpg --verify V-20170317.tar.gz.mod6.sig V-20170317.tar.gz &&\
  tar xf V-20170317.tar.gz &&\
  gpg --verify v.pl.mod6.sig v.pl &&\
  gpg --verify v_quick_start.txt.mod6.sig v_quick_start.txt &&\
  gpg --verify v_users_manual.txt.mod6.sig v_users_manual.txt &&\
  ln -s /w/v/v.pl /bin/v \
  || exit 1

WORKDIR /w/patches
ADD ./patches /w/patches
#wget http://www.loper-os.org/pub/ffa/ffa_ch1_genesis.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch2_logicals.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch3_shifts.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch4_ffacalc.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch5_egypt.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch6_simplest_rsa.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch7_turbo_egyptians.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch8_randomism.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch9_exodus.vpatch &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch10_karatsuba.vpatch &&\
RUN sha256sum * | diff - .sha256sums \
  || exit 1

WORKDIR /w/.seals
ADD ./.seals /w/.seals
#wget http://www.loper-os.org/pub/ffa/ffa_ch1_genesis.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch2_logicals.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch3_shifts.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch4_ffacalc.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch5_egypt.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch6_simplest_rsa.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch7_turbo_egyptians.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch8_randomism.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch9_exodus.vpatch.asciilifeform.sig &&\
#wget http://www.loper-os.org/pub/ffa/ffa_ch10_karatsuba.vpatch.asciilifeform.sig &&\
RUN sha256sum * | diff - .sha256sums \
  || exit 1

WORKDIR /w/ffa
ADD ./ffa /w/ffa

WORKDIR /w
ADD ./no_fdump_scos.patch /w
RUN \
  for i in `ls patches`; do gpg --verify .seals/$i.asciilifeform.sig patches/$i; done &&\
  v p ffa ffa_ch10_karatsuba.vpatch &&\
  patch -p0 < no_fdump_scos.patch &&\
  cd ffa/ffa/ffacalc &&\
  gprbuild \
  || exit 1

CMD ["/bin/bash"]

