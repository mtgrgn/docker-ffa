diff -uNr a/ffa/ffacalc/ffa_calc.adb b/ffa/ffacalc/ffa_calc.adb
--- a/ffa/ffacalc/ffa_calc.adb a94fda8f8e4c698acdd894f5f7b10d927a4050aba96096ccb0893b668ea1ba789389b00b62cd997e2617399fa6e2ea969f6adb4e7bf31c10cc34b74f2e3978de
+++ b/ffa/ffacalc/ffa_calc.adb 38d0d627d069d4dd52809d1d4e1f2bd210ea677d65d87fe64a468a1532841bbefb472716c12e7f6bb8595914d0a53604cfdbc69a42c408c6a2f3809cc79896d6
@@ -32,6 +32,8 @@
 with FZ_Pred;  use FZ_Pred;
 with FZ_BitOp; use FZ_BitOp;
 with FZ_Shift; use FZ_Shift;
+with FZ_Divis; use FZ_Divis;
+with FZ_Mul;   use FZ_Mul;
 
 -- For Output
 with FFA_IO;   use FFA_IO;
@@ -147,6 +149,15 @@
       end Want;
       
       
+      -- Ensure that a divisor is not zero
+      procedure MustNotZero(D : in FZ) is
+      begin
+         if FZ_ZeroP(D) = 1 then
+            E("Division by Zero!");
+         end if;
+      end MustNotZero;
+      
+      
       -- Slide a new hex digit into the FZ on top of stack
       procedure Ins_Hex_Digit(N : in out FZ;
                               D : in Nibble) is
@@ -316,6 +327,43 @@
                Flag := W_NZeroP(F);
                Drop;
                
+               -- Divide and give Quotient and Remainder
+            when '\' =>
+               Want(2);
+               MustNotZero(Stack(SP));
+               FZ_IDiv(Dividend  => Stack(SP - 1),
+                       Divisor   => Stack(SP),
+                       Quotient  => Stack(SP - 1),
+                       Remainder => Stack(SP));
+               
+               -- Divide and give Quotient only
+            when '/' =>
+               Want(2);
+               MustNotZero(Stack(SP));
+               FZ_Div(Dividend  => Stack(SP - 1),
+                      Divisor   => Stack(SP),
+                      Quotient  => Stack(SP - 1));
+               Drop;
+               
+               -- Divide and give Remainder only
+            when '%' =>
+               Want(2);
+               MustNotZero(Stack(SP));
+               FZ_Mod(Dividend  => Stack(SP - 1),
+                      Divisor   => Stack(SP),
+                      Remainder => Stack(SP - 1));
+               Drop;
+               
+               -- Multiply, give bottom and top halves
+            when '*' =>
+               Want(2);
+               MustNotZero(Stack(SP));
+               -- Ch5: slow and simple 'Egyptological' method:
+               FZ_Mul_Egyptian(X     => Stack(SP - 1),
+                               Y     => Stack(SP),
+                               XY_Lo => Stack(SP - 1),
+                               XY_Hi => Stack(SP));
+               
                -----------------
                -- Bitwise Ops --
                -----------------
diff -uNr a/ffa/libffa/fz_arith.adb b/ffa/libffa/fz_arith.adb
--- a/ffa/libffa/fz_arith.adb 02733393deff85e06f21bf085b23af379a357cb60143c6efb6d778b523ca3b4d37e27d9db625751e0fbe5c811a47e72fc6f3185093d693d7541fd4267ce158c7
+++ b/ffa/libffa/fz_arith.adb 7426e4888bf866d3e8b14a4e03a96f4c4cae79d13f4d6bb5857aeb17b5febdc17656ad496c635f9123e0791ba8e18a9e83106900bf817ba582b6a36683474ad3
@@ -44,6 +44,44 @@
    pragma Inline_Always(FZ_Add);
    
    
+   -- Gate = 1: Sum := X + Y; Overflow := Carry
+   -- Gate = 0: Sum := X;     Overflow := 0
+   procedure FZ_Add_Gated_O(X          : in  FZ;
+                            Y          : in  FZ;
+                            Gate       : in  WBool;
+                            Sum        : out FZ;
+                            Overflow   : out WBool) is
+      Carry : WBool := 0;
+      Mask  : constant Word := 0 - Gate;
+   begin
+      for i in 0 .. Word_Index(X'Length - 1) loop
+         declare
+            A : constant Word := X(X'First + i);
+            B : constant Word := Y(Y'First + i) and Mask;
+            S : constant Word := A + B + Carry;
+         begin
+            Sum(Sum'First + i) := S;
+            Carry  := W_Carry(A, B, S);
+         end;
+      end loop;
+      Overflow := Carry;
+   end FZ_Add_Gated_O;
+   pragma Inline_Always(FZ_Add_Gated_O);
+   
+   
+   -- Same as FZ_Add_Gated_O, but without Overflow output
+   procedure FZ_Add_Gated(X          : in  FZ;
+                          Y          : in  FZ;
+                          Gate       : in  WBool;
+                          Sum        : out FZ) is
+      Overflow : Word;
+      pragma Unreferenced(Overflow);
+   begin
+      FZ_Add_Gated_O(X, Y, Gate, Sum, Overflow);
+   end FZ_Add_Gated;
+   pragma Inline_Always(FZ_Add_Gated);
+   
+   
    -- Difference := X - Y; Underflow := Borrow
    procedure FZ_Sub(X          : in  FZ;
                     Y          : in  FZ;
diff -uNr a/ffa/libffa/fz_arith.ads b/ffa/libffa/fz_arith.ads
--- a/ffa/libffa/fz_arith.ads 2179915542fa14c44a706f52a9d72556f7085423404304b4c3321b1b5e3e07f1b33674a752d3aab7ef108c913ef9a93166644dc5692ee766744ac68eb1cefaad
+++ b/ffa/libffa/fz_arith.ads c00baef0b2e192924f96d2b8091d607fc82787d4d6bc97463edb3a6e317b0292bcb8a0b1f93bc410fecde02a6763b785503fab4e6be4439b8dad8cec350f4882
@@ -32,6 +32,22 @@
                     Overflow   : out   WBool);
    pragma Precondition(X'Length = Y'Length and X'Length = Sum'Length);
    
+   -- Gate = 1: Sum := X + Y; Overflow := Carry
+   -- Gate = 0: Sum := X;     Overflow := 0
+   procedure FZ_Add_Gated_O(X          : in  FZ;
+                            Y          : in  FZ;
+                            Gate       : in  WBool;
+                            Sum        : out FZ;
+                            Overflow   : out WBool);
+   pragma Precondition(X'Length = Y'Length and X'Length = Sum'Length);
+   
+   -- Same as FZ_Add_Gated_O, but without Overflow output
+   procedure FZ_Add_Gated(X          : in  FZ;
+                          Y          : in  FZ;
+                          Gate       : in  WBool;
+                          Sum        : out FZ);
+   pragma Precondition(X'Length = Y'Length and X'Length = Sum'Length);
+   
    -- Difference := X - Y; Underflow := Borrow
    procedure FZ_Sub(X          : in  FZ;
                     Y          : in  FZ;
diff -uNr a/ffa/libffa/fz_basic.adb b/ffa/libffa/fz_basic.adb
--- a/ffa/libffa/fz_basic.adb 9f84b8baa6ebd696cd3e4c924e7c23404fea9b5f2b950139b9b133dbb5698b6e3304f0c64e9b4ff639be287852c88e293074931423ca16259d3c5060f6da7927
+++ b/ffa/libffa/fz_basic.adb 7ed87df44916cbef43b37a703f63dba05ba3c48db81bf531f195224fc874950430b5603d81595152ec66fd9a36427bc6a02c66323f3f85769e8830604793797d
@@ -26,6 +26,14 @@
    -- Fundamental Operations on FZ (finite integers)
    ---------------------------------------------------------------------------
    
+   -- Determine the Bitness of N
+   function FZ_Bitness(N : in FZ) return Bit_Count is
+   begin
+      return N'Length * Words.Bitness;
+   end FZ_Bitness;
+   pragma Inline_Always(FZ_Bitness);
+   
+   
    -- N := 0
    procedure FZ_Clear(N : out FZ) is
    begin
diff -uNr a/ffa/libffa/fz_basic.ads b/ffa/libffa/fz_basic.ads
--- a/ffa/libffa/fz_basic.ads 8ad840a435cba8610c3e1eebc5db1c4c4e59f849b2c7b970a433c21976813c2b854e5d1e056f13cd1066faf38985bd0fba1c49aab873722222ad46942992cb90
+++ b/ffa/libffa/fz_basic.ads 25241e9b59d58d8d3a580cf862ff136be1b02ffd547a943c3e2e7ecfd34d84cd059939253394f400f9e26d5be1775f73cc755636572332b060137e270c41943a
@@ -25,6 +25,9 @@
    
    pragma Pure;
    
+   -- Determine the Bitness of N
+   function FZ_Bitness(N : in FZ) return Bit_Count;
+   
    -- N := 0
    procedure FZ_Clear(N : out FZ);
    
diff -uNr a/ffa/libffa/fz_divis.adb b/ffa/libffa/fz_divis.adb
--- a/ffa/libffa/fz_divis.adb false
+++ b/ffa/libffa/fz_divis.adb a955190427a73c83dd3abb5cf4f547d6cd29022bf193bfa3403edf4c227e9917a933c2eeb26c2dcc2df642292880cdfe051ce21349afce656cf9d66ceee74c7a
@@ -0,0 +1,96 @@
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+-- This file is part of 'Finite Field Arithmetic', aka 'FFA'.               --
+--                                                                          --
+-- (C) 2017 Stanislav Datskovskiy ( www.loper-os.org )                      --
+-- http://wot.deedbot.org/17215D118B7239507FAFED98B98228A001ABFFC7.html     --
+--                                                                          --
+-- You do not have, nor can you ever acquire the right to use, copy or      --
+-- distribute this software ; Should you use this software for any purpose, --
+-- or copy and distribute it to anyone or in any manner, you are breaking   --
+-- the laws of whatever soi-disant jurisdiction, and you promise to         --
+-- continue doing so for the indefinite future. In any case, please         --
+-- always : read and understand any software ; verify any PGP signatures    --
+-- that you use - for any purpose.                                          --
+--                                                                          --
+-- See also http://trilema.com/2015/a-new-software-licensing-paradigm .     --
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+
+with Words;    use Words;
+with W_Pred;   use W_Pred;
+with FZ_Basic; use FZ_Basic;
+with FZ_Arith; use FZ_Arith;
+with FZ_BitOp; use FZ_BitOp;
+with FZ_Shift; use FZ_Shift;
+
+
+package body FZ_Divis is
+   
+   -- Dividend is divided by Divisor, producing Quotient and Remainder.
+   -- WARNING: NO div0 test here! Caller must test.
+   procedure FZ_IDiv(Dividend  : in  FZ;
+                     Divisor   : in  FZ;
+                     Quotient  : out FZ;
+                     Remainder : out FZ) is
+      
+      -- The working register
+      QR : FZ(1 .. Dividend'Length + Divisor'Length);
+      
+      -- Bottom seg of Z will end up containing the Quotient; top - remainder
+      Q  : FZ renames QR(1                   .. Dividend'Length); -- Quotient
+      R  : FZ renames QR(Dividend'Length + 1 .. QR'Last);          -- Remainder
+      
+      C : WBool := 0; -- Borrow, from comparator
+   begin
+      Q := Dividend; -- Q begins with the Dividend
+      FZ_Clear(R);   -- R begins empty
+      
+      -- For each bit of Dividend:
+      for i in 1 .. FZ_Bitness(Dividend) loop
+         
+         -- Advance QR by 1 bit:
+         FZ_ShiftLeft(QR, QR, 1);
+         
+         -- Subtract Divisor from R; Underflow goes into C
+         FZ_Sub(X => R, Y => Divisor, Difference => R, Underflow => C);
+         
+         -- If C=1, subtraction underflowed, and then Divisor gets added back:
+         FZ_Add_Gated(X => R, Y => Divisor, Gate => C, Sum => R);
+         
+         -- Current result-bit is equal to Not-C, i.e. 1 if Divisor 'went in'
+         FZ_Or_W(Q, W_Not(C));
+         
+      end loop;
+      
+      Quotient  := Q; -- Output the Quotient.
+      Remainder := R; -- Output the Remainder.
+      
+   end FZ_IDiv;
+   pragma Inline_Always(FZ_IDiv);
+   
+   
+   -- Exactly same thing as IDiv, but keep only the Quotient
+   procedure FZ_Div(Dividend  : in  FZ;
+                    Divisor   : in  FZ;
+                    Quotient  : out FZ) is
+      Remainder : FZ(Divisor'Range);
+      pragma Unreferenced(Remainder);
+   begin
+      FZ_IDiv(Dividend, Divisor, Quotient, Remainder);
+   end FZ_Div;
+   pragma Inline_Always(FZ_Div);
+   
+   
+   -- Exactly same thing as IDiv, but keep only the Remainder
+   procedure FZ_Mod(Dividend  : in FZ;
+                    Divisor   : in FZ;
+                    Remainder : out FZ) is
+      Quotient : FZ(Dividend'Range);
+      pragma Unreferenced(Quotient);
+   begin
+      FZ_IDiv(Dividend, Divisor, Quotient, Remainder);
+   end FZ_Mod;
+   pragma Inline_Always(FZ_Mod);
+   
+end FZ_Divis;
diff -uNr a/ffa/libffa/fz_divis.ads b/ffa/libffa/fz_divis.ads
--- a/ffa/libffa/fz_divis.ads false
+++ b/ffa/libffa/fz_divis.ads 9caf188054443b94a2ac70cb5c344367cbf6450a3d0c590b86275cfeb75a20b862eca82ac047362d051913c684465c0c32656293fbefb883da06f5232f910599
@@ -0,0 +1,51 @@
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+-- This file is part of 'Finite Field Arithmetic', aka 'FFA'.               --
+--                                                                          --
+-- (C) 2017 Stanislav Datskovskiy ( www.loper-os.org )                      --
+-- http://wot.deedbot.org/17215D118B7239507FAFED98B98228A001ABFFC7.html     --
+--                                                                          --
+-- You do not have, nor can you ever acquire the right to use, copy or      --
+-- distribute this software ; Should you use this software for any purpose, --
+-- or copy and distribute it to anyone or in any manner, you are breaking   --
+-- the laws of whatever soi-disant jurisdiction, and you promise to         --
+-- continue doing so for the indefinite future. In any case, please         --
+-- always : read and understand any software ; verify any PGP signatures    --
+-- that you use - for any purpose.                                          --
+--                                                                          --
+-- See also http://trilema.com/2015/a-new-software-licensing-paradigm .     --
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+
+with FZ_Type; use FZ_Type;
+
+
+package FZ_Divis is
+   
+   pragma Pure;
+   
+   -- Dividend is divided by Divisor, producing Quotient and Remainder.
+   -- WARNING: NO div0 test here! Caller must test.
+   procedure FZ_IDiv(Dividend  : in  FZ;
+                     Divisor   : in  FZ;
+                     Quotient  : out FZ;
+                     Remainder : out FZ);
+   pragma Precondition(Dividend'Length = Divisor'Length and
+                         Quotient'Length = Remainder'Length and
+                         Dividend'Length = Quotient'Length);
+   
+   -- Exactly same thing as IDiv, but keep only the Quotient
+   procedure FZ_Div(Dividend  : in FZ;
+                    Divisor   : in FZ;
+                    Quotient  : out FZ);
+   pragma Precondition(Dividend'Length = Divisor'Length and
+                         Dividend'Length = Quotient'Length);
+   
+   -- Exactly same thing as IDiv, but keep only the Remainder
+   procedure FZ_Mod(Dividend  : in FZ;
+                    Divisor   : in FZ;
+                    Remainder : out FZ);
+   pragma Precondition(Dividend'Length = Divisor'Length and
+                         Dividend'Length = Remainder'Length);
+   
+end FZ_Divis;
diff -uNr a/ffa/libffa/fz_lim.adb b/ffa/libffa/fz_lim.adb
--- a/ffa/libffa/fz_lim.adb cfc75db07fa645da2fd4c42cd9923c6ea760d4d0f8182624a91e1b88b2cab4b7ccb227b371f5f02d46382a9cff92402a6885f74b0dcb7008196a8a29cd27fa4b
+++ b/ffa/libffa/fz_lim.adb 76024b01d3e0ec671b985c2affcf0510c4a2dc9667d637111d5bc191678eb6e8a7da124fbe667a46db5a6e1e5f4963519d537e3b74c68482f75c9417f692f98e
@@ -28,9 +28,7 @@
       -- Supposing we meet the minimal bitness:
       if B >= FZ_Minimal_Bitness then
          while T > 0 loop
-            if T mod 2 = 1 then
-               PopCount := PopCount + 1;
-            end if;
+            PopCount := PopCount + T mod 2;
             T := T / 2;
          end loop;
          
diff -uNr a/ffa/libffa/fz_mul.adb b/ffa/libffa/fz_mul.adb
--- a/ffa/libffa/fz_mul.adb false
+++ b/ffa/libffa/fz_mul.adb ed4f3d12f180e3a39bc7ffc6b17f488f29087c490969a14c9b25c0dd076a8920d0e59f8d0768f433dade6d0c5ccf1022226f96b54b4efe1bc7d26b3d5f24efda
@@ -0,0 +1,74 @@
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+-- This file is part of 'Finite Field Arithmetic', aka 'FFA'.               --
+--                                                                          --
+-- (C) 2017 Stanislav Datskovskiy ( www.loper-os.org )                      --
+-- http://wot.deedbot.org/17215D118B7239507FAFED98B98228A001ABFFC7.html     --
+--                                                                          --
+-- You do not have, nor can you ever acquire the right to use, copy or      --
+-- distribute this software ; Should you use this software for any purpose, --
+-- or copy and distribute it to anyone or in any manner, you are breaking   --
+-- the laws of whatever soi-disant jurisdiction, and you promise to         --
+-- continue doing so for the indefinite future. In any case, please         --
+-- always : read and understand any software ; verify any PGP signatures    --
+-- that you use - for any purpose.                                          --
+--                                                                          --
+-- See also http://trilema.com/2015/a-new-software-licensing-paradigm .     --
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+
+with Words;    use Words;
+with FZ_Basic; use FZ_Basic;
+with FZ_Pred;  use FZ_Pred;
+with FZ_Arith; use FZ_Arith;
+with FZ_Shift; use FZ_Shift;
+
+
+package body FZ_Mul is
+   
+   -- 'Egyptological' multiplier. XY_Lo and XY_Hi hold result of X*Y.
+   procedure FZ_Mul_Egyptian(X     : in  FZ;
+                             Y     : in  FZ;
+                             XY_Lo : out FZ;
+                             XY_Hi : out FZ) is
+      
+      -- Register holding running product
+      XY : FZ(1 .. X'Length + Y'Length);
+      
+      -- X-Slide
+      XS : FZ(1 .. X'Length + Y'Length);
+      
+      -- Y-Slide
+      YS : FZ(Y'Range) := Y;
+      
+   begin
+      -- Product register begins empty
+      FZ_Clear(XY);
+      
+      -- X-Slide initially equals X:
+      XS(1            .. X'Length) := X;
+      XS(X'Length + 1 .. XS'Last)  := (others => 0);
+      
+      -- For each bit of Y:
+      for i in 1 .. FZ_Bitness(Y) loop
+         
+         -- If lowest bit of Y-Slide is 1, X-Slide is added into XY
+         FZ_Add_Gated(X    => XY, Y => XS, Sum => XY,
+                      Gate => FZ_OddP(YS));
+         
+         -- X-Slide := X-Slide * 2
+         FZ_ShiftLeft(XS, XS, 1);
+         
+         -- Y-Slide := Y-Slide / 2
+         FZ_ShiftRight(YS, YS, 1);
+         
+      end loop;
+      
+      -- Write out the Product's lower and upper FZs:
+      XY_Lo := XY(1                .. XY_Lo'Length);
+      XY_Hi := XY(XY_Lo'Length + 1 .. XY'Last);
+      
+   end FZ_Mul_Egyptian;
+   pragma Inline_Always(FZ_Mul_Egyptian);
+
+end FZ_Mul;
diff -uNr a/ffa/libffa/fz_mul.ads b/ffa/libffa/fz_mul.ads
--- a/ffa/libffa/fz_mul.ads false
+++ b/ffa/libffa/fz_mul.ads 754392542e46ec3ce2661f8cf10d88ad76defe90c0634d885ca6ed8beb621a96c4ecf7eccd90bcb168651e60f8a4a431cb9b62d220f4ed61a7d69e94b6b360da
@@ -0,0 +1,36 @@
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+-- This file is part of 'Finite Field Arithmetic', aka 'FFA'.               --
+--                                                                          --
+-- (C) 2017 Stanislav Datskovskiy ( www.loper-os.org )                      --
+-- http://wot.deedbot.org/17215D118B7239507FAFED98B98228A001ABFFC7.html     --
+--                                                                          --
+-- You do not have, nor can you ever acquire the right to use, copy or      --
+-- distribute this software ; Should you use this software for any purpose, --
+-- or copy and distribute it to anyone or in any manner, you are breaking   --
+-- the laws of whatever soi-disant jurisdiction, and you promise to         --
+-- continue doing so for the indefinite future. In any case, please         --
+-- always : read and understand any software ; verify any PGP signatures    --
+-- that you use - for any purpose.                                          --
+--                                                                          --
+-- See also http://trilema.com/2015/a-new-software-licensing-paradigm .     --
+------------------------------------------------------------------------------
+------------------------------------------------------------------------------
+
+with FZ_Type; use FZ_Type;
+
+
+package FZ_Mul is
+   
+   pragma Pure;
+   
+   -- 'Egyptological' multiplier. XY_Lo and XY_Hi hold result of X*Y.
+   procedure FZ_Mul_Egyptian(X     : in  FZ;
+                             Y     : in  FZ;
+                             XY_Lo : out FZ;
+                             XY_Hi : out FZ);
+   pragma Precondition(X'Length = Y'Length and
+                         XY_Lo'Length = XY_Hi'Length and
+                         XY_Lo'Length = ((X'Length + Y'Length) / 2));
+   
+end FZ_Mul;
diff -uNr a/ffa/libffa/fz_type.ads b/ffa/libffa/fz_type.ads
--- a/ffa/libffa/fz_type.ads eb4315dc5cbd51be99ad3575f8a67850c58c642e431dd81369c207d45f248e2590b3bcf7ae77749031b9b5e6c900026b7273ecd858c40cb0d75d18acc9978e9b
+++ b/ffa/libffa/fz_type.ads af6a289ef862f6591a5e88f58871447c325476b4e3d89bdb2cb5b2b42a86e0df35fa3ae85debff1ff89af69ce60b0839842cb5331b01715671c611b42a92ca6e
@@ -41,6 +41,9 @@
    -- A count of Words in an FZ (cannot be 0):
    subtype Word_Count is Indices range 1 .. Indices'Last;
    
+   -- A count of Bits, anywhere (cannot be 0):
+   subtype Bit_Count is Positive;
+   
    -- An index of a particular ~bit~ in an FZ:
    subtype FZBit_Index is Indices;
    
diff -uNr a/ffa/libffa/w_pred.adb b/ffa/libffa/w_pred.adb
--- a/ffa/libffa/w_pred.adb c2d77552063fbd97f887509c4ef741b8e3c623c12818c4c4c3ca4d061232f053e254b10acc4e9e0a9d994f990e4eb13371ba6c61fad363e8017af8ef8b829503
+++ b/ffa/libffa/w_pred.adb 3a15947854b3a13a8fbc1caa660c4ff8fe58d6d1fbde92d18a7609b96a4c5c8a8cde444acf80832c77b32f99a2f8f293478a7873fa5087a4afbcf8c1889beaf4
@@ -38,6 +38,14 @@
    pragma Inline_Always(W_NZeroP);
    
    
+   -- Return WBool-complement of N.
+   function W_Not(N : in WBool) return WBool is
+   begin
+      return 1 xor N;
+   end W_Not;
+   pragma Inline_Always(W_Not);
+   
+   
    -- Return 1 if N is odd; otherwise return 0.
    function W_OddP(N : in Word) return WBool is
    begin
diff -uNr a/ffa/libffa/w_pred.ads b/ffa/libffa/w_pred.ads
--- a/ffa/libffa/w_pred.ads 5741da7808ac201910fabe630437419af19d3d1f76ee9de49594a6bed499c84a9d7cfd2491b4a13943b94798dbcfe4f79193559dd3338189ed9cfa281a2fbe2a
+++ b/ffa/libffa/w_pred.ads 8452a69859a3c4fd7e67904fb3505705433e76deb3e127322b70d5c00e65e5ad32eb3c7636baf3079948be424b98f86d4fb2ac2769cc8b982e69d8f58143a0c5
@@ -30,6 +30,9 @@
    -- Return 1 if N is unequal to 0; otherwise return 0.
    function W_NZeroP(N : in Word) return WBool;
    
+   -- Return WBool-complement of N.
+   function W_Not(N : in WBool) return WBool;
+   
    -- Return 1 if N is odd; otherwise return 0.
    function W_OddP(N : in Word) return WBool;
    
